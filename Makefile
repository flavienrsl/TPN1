bin/VBO: src/VBO.c include/color.h include/point.h
	gcc -o bin/VBO src/VBO.c -Iinclude -lglut -lGL -lGLU

bin/texcubeIL1: src/texcube.c
	gcc -o bin/texcubeIL1 src/texcube.c -lIL -std=c99 -lglut -lGLU -lGL -lm

bin/texcubeIL2: src/texcube.c
	gcc -o bin/texcubeIL2 src/texcube.c -DIL_VERSION -lIL -std=c99 -lglut -lGLU -lGL -lm

clean:
	rm -rf bin/*

all: bin/VBO bin/texcubeIL1 bin/texcubeIL2
